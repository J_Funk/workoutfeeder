fs = require "fs"
imap = require "imap"

class FeedEmailServer

  constructor: ->
    @startEmailServer()

  startEmailServer: ->
    @emailCredentialsConfig = JSON.parse fs.readFileSync "#{process.cwd()}/config.json", "utf-8"
    @emailServer = new imap.ImapConnection
      username: @emailCredentialsConfig.username
      password: @emailCredentialsConfig.password
      host: @emailCredentialsConfig.imap.host
      port: @emailCredentialsConfig.imap.port
      secure: @emailCredentialsConfig.imap.secure


  exitOnErr: (err) ->
    console.error err
    do process.exit

  openInbox: ->
    @emailServer.openBox "INBOX", false, (err, box) ->
      @exitOnErr err if err
      console.log "You have #{box.messages.total} messages in your INBOX"
      @fetchEmailsFromConfiguredAddress()


  fetchEmailsFromConfiguredAddress: ->
    @emailServer.search ["UNSEEN", ["SINCE", "Sep 18, 2011"], ["FROM", @emailCredentialsConfig.email]], (err, results) ->
      @exitOnErr err if err
      unless results.length
          console.log "No unread messages from #{@emailCredentialsConfig.email}"
          do @emailServer.logout
          return
      fetch = @emailServer.fetch results,
          request:
              body: "full"
              headers: false
      fetch.on "message", (message) ->
          fds = {}
          filenames = {}
          parser = new mailparser.MailParser
          parser.on "headers", (headers) ->
              console.log "Message: #{headers.subject}"
          parser.on "astart", (id, headers) ->
              filenames[id] = headers.filename
              fds[id] = fs.openSync headers.filename, 'w'
          parser.on "astream", (id, buffer) ->
              fs.writeSync fds[id], buffer, 0, buffer.length, null
          parser.on "aend", (id) ->
              return unless fds[id]
              fs.close fds[id], (err) ->
                  return console.error err if err
                  console.log "Writing #{filenames[id]} completed"
          message.on "data", (data) ->
              parser.feed data.toString()
          message.on "end", ->
              do parser.end
      fetch.on "end", ->
          do @emailServer.logout

  connect: ->
    @emailServer.connect (error) ->
      @exitOnErr err if err      
      @openInbox()
          

   