FeedParser = require 'feedparser'
mailparser = require "mailparser"
emailServer = require "FeedEmailServer.coffee"

class Feeder 

  constructor: ->
    @parser = new FeedParser()
    @startEmailServer()
    @parser.parseUrl 'http://crossfitemerge.rxgymsoftware.com/rsswod.asp', @readFeed

  isArticleFromToday: (article) -> 
    article.meta.pubdate? and article.meta.pubdate.getTime() >=  @getToday()

  getToday: ->
    now = new Date()
    now.setHours 0
    now.setMinutes 0
    now.setSeconds 0
    now.setMilliseconds 0
    now.getTime()

  readFeed: (error, meta, articles) =>
    articles.forEach (article) =>
      if @isArticleFromToday article then @emailArticle article

  emailArticle: (article) ->
    console.log article.summary

new Feeder()